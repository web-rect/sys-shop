

const menuM = document.querySelector('.menu-m'),
modalMenuOpen = document.querySelector('.modal-menu-open'),
modalMenuPpenBg = document.querySelector('.modal-menu-open-bg'),
modalMenuClose = document.querySelector('.modal-menu-close');


if (menuM) {
    menuM.addEventListener('click', () => {
        modalMenuOpen.classList.toggle('active')
    })
    modalMenuPpenBg.addEventListener('click', () => {
        modalMenuOpen.classList.remove('active')
    })
    modalMenuClose.addEventListener('click', () => {
        modalMenuOpen.classList.remove('active')
    })
}


const headerDrop = document.querySelector('.header-drop'),
headerMenuTop = document.querySelector('.header-menu .menu');

let activeMenuDrop = false

if (headerMenuTop) {
    headerMenuTop.addEventListener('click', (e) => {
        e.preventDefault();
        if (activeMenuDrop) {
            activeMenuDrop = false 
            headerDrop.classList.remove('active')  
            headerMenuTop.textContent = 'закрыть'
            
        } else {
            activeMenuDrop = true
            headerDrop.classList.add('active')  
            headerMenuTop.textContent = 'меню'
           
        }
        
    })
    headerMenuTop.addEventListener('mouseover', () => {
        if (activeMenuDrop) {
            headerMenuTop.textContent = 'закрыть'
        } else {
            headerMenuTop.textContent = 'открыть'
        }
       
    })
    headerMenuTop.addEventListener('mouseout', () => {
        if (activeMenuDrop) {
            headerMenuTop.textContent = 'закрыть'
        } else {
            headerMenuTop.textContent = 'меню'
        }
        
    })
}

const modalPhone = document.querySelectorAll('.md-open'),
modalPhoneBg = document.querySelectorAll('.modal-phone-bg'),
modalPhoneClose = document.querySelectorAll('.modal-phone-close'),
cartJs = document.querySelector('.cart-js'),
modalCarts = document.querySelector('.modal-carts');

if (cartJs) {
    cartJs.addEventListener('click', (e) => {
        e.preventDefault()
        modalCarts.classList.add('active')
    })
}

if (modalPhoneBg) {
    modalPhoneBg.forEach(item => {
        item.addEventListener('click', () => {
            console.log('yes');
            modalPhone.forEach(elem => {
                elem.classList.remove('active')
            })
        })
    })
    modalPhoneClose.forEach(item => {
        item.addEventListener('click', () => {
            modalPhone.forEach(elem => {
                elem.classList.remove('active')
            })
        })
    })
    
}

const search = document.querySelector('.search'),
searchImg = document.querySelector('.search img'),
headerLogo = document.querySelector('.header-logo');

let param = false

if (search) {
    searchImg.addEventListener('click', e => {
        e.preventDefault()
        if (param) {
            param = false
        } else {
            param = true
        }
        if (param) {
            search.classList.add('active')
            headerLogo.classList.add('active')
        } else {
            search.classList.remove('active')
            headerLogo.classList.remove('active')
        }
        
    })

}

const modalProductClose = document.querySelector('.modal-product-close'),
filtrM = document.querySelector('.filtr-m'),
modalProduct = document.querySelector('.modal-product'),
modalProductBg = document.querySelector('.modal-product-bg');

if (filtrM) {
    filtrM.addEventListener('click', () => {
        modalProduct.classList.add('active')
    })
    modalProductClose.addEventListener('click', () => {
        modalProduct.classList.remove('active')
    })
    modalProductBg.addEventListener('click', () => {
        modalProduct.classList.remove('active')
    })
}

const deliverTabsMTitle = document.querySelector('.deliver-tabs-m .title'),
deliverTabsM = document.querySelector('.deliver-tabs-m'),
deliverTabsSpan = document.querySelectorAll('.deliver-tabs-m .drop span');

if (deliverTabsMTitle) {
    deliverTabsMTitle.addEventListener('click', () => {
        deliverTabsM.classList.toggle('active')
    })
    deliverTabsSpan.forEach(item => {
        item.addEventListener('click', () => {
            deliverTabsM.classList.remove('active')
        })
    })
}


const deliverTabs = document.querySelectorAll('.deliver-tabs .item'),
deliverTabItem = document.querySelectorAll('.deliver-tab-item'),
deliverTabsMChange = document.querySelectorAll('.deliver-tabs-m .drop span'),
deliverTabsTitle = document.querySelector('.deliver-tabs-m .title span');

let countTab = 0

if (deliverTabs[0]) {

    const changeActiveTab = () => {
        deliverTabItem.forEach(item => {
            item.classList.remove('active')
        })
        deliverTabs.forEach(item => {
            item.classList.remove('active')
        })
        deliverTabs[countTab].classList.add('active')
        deliverTabItem[countTab].classList.add('active')
        deliverTabsTitle.textContent = deliverTabsMChange[countTab].textContent
    }
    changeActiveTab()

    deliverTabs.forEach((item, index) => {
        item.addEventListener('click', () => {
            countTab = index
            changeActiveTab();
        })
   
    })
    deliverTabsMChange.forEach((item, index) => {
        item.addEventListener('click', () => {
            countTab = index
            changeActiveTab();
        })
    })

   

}




const $checks = document.querySelectorAll('.check-in-js');


if ($checks) {
    $checks.forEach(item => {
        
        item.addEventListener('click', (e) => {
            e.preventDefault()
            if (item.getAttribute('active')) {
                
                if (e.target.closest('.elem-title')) {
                 
                    $checks.forEach(elem => {
                        elem.classList.remove('active');
                        elem.removeAttribute('active');
                    });
                    item.classList.remove('active');
                    item.removeAttribute('active');
                }
               
            } else {
                if (e.target.closest('.elem-title')) {
                    $checks.forEach(elem => {
                        elem.classList.remove('active');
                        elem.removeAttribute('active');
                    });
                    item.classList.add('active');
                    item.setAttribute('active', 'active');
                }
                
            }
        });
    });
}


const textMsize = document.querySelector('.text-m-size'),
mReadBtn = document.querySelector('.m-read-btn');

if (mReadBtn) {
    mReadBtn.addEventListener('click', () => {
        mReadBtn.classList.toggle('active')
        textMsize.classList.toggle('active')
    })
}


const checkerTab = document.querySelectorAll('.checker-tab');

if (checkerTab) {
    checkerTab.forEach(item => {
        item.addEventListener('click', () => {
            checkerTab.forEach(elem => {
                elem.classList.remove('active')
            })
            item.classList.toggle('active')
        })
    })
}

const changeCountProduct = document.querySelector('.change-count-product');

let countProduct = 1

if (changeCountProduct) {
    changeCountProduct.querySelector('.ar-left').addEventListener('click', () => {
        if (countProduct >= 2) {
            countProduct--
            changeCountText()
        }
    })
    changeCountProduct.querySelector('.ar-right').addEventListener('click', () => {
        countProduct++
        changeCountText()
    })
    const changeCountText = () => {
        changeCountProduct.querySelector('.start-count').textContent = countProduct
    }
    changeCountText()
}

const dropdView = document.querySelector('.dropd-view');

if (dropdView) {
    dropdView.querySelector('.dropdwn-title').addEventListener('click', () => {
        dropdView.classList.toggle('active')
    })   
    dropdView.querySelectorAll('.dropdwn-content .item').forEach(item => {
        item.addEventListener('click', () => {
            dropdView.querySelector('.dropdwn-title span').textContent = item.textContent
            dropdView.classList.remove('active')
        })
    })
}

const phoneJsVis = document.querySelector('.phone-js-vis');

if (phoneJsVis) {
    phoneJsVis.addEventListener('click', () => {
        document.querySelector('.modal-phone-one').classList.add('active')
    })
}

const moreParamsItems = document.querySelector('.more-params-items');

if (moreParamsItems) {
    moreParamsItems.querySelectorAll('.item span').forEach(item => {
        item.addEventListener('click', () => {
            moreParamsItems.querySelectorAll('.item span').forEach(item => {
                item.classList.remove('active')
            })
            item.classList.toggle('active')
        })
    })
}

const countPaginationSection = document.querySelectorAll('.count-pagination-section');

let countPagination = `${1}/${2}`


const swiper = new Swiper('.swiper-container', {
    // Default parameters
    slidesPerView: 1,
    spaceBetween: 10,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    pagination: {
        el: ".swiper-pagination-section",
        type: "fraction",
    },
    breakpoints: {
        // when window width is >= 320px
        640: {
            slidesPerView: 1.4,
            spaceBetween: 20
        },
        1080: {
            slidesPerView: 1,
            spaceBetween: 20
        }
    },
    on: {
        slideChange: function(param) {
            countPagination = ''
            countPagination = `${param.activeIndex + 1}/${param.slides.length}`
            updateCountPag()
        }
    }
    
})

const updateCountPag = () => {
    if (countPaginationSection) {
        countPaginationSection.forEach(item => {
            item.textContent = countPagination
        })
    }

}
updateCountPag()



const swiperPos = new Swiper('.swiper-container-pos', {
    // Default parameters
    slidesPerView: 4,
    spaceBetween: 10,
    navigation: {
        nextEl: '.swiper-button-next-pos',
        prevEl: '.swiper-button-prev-pos',
    },
    pagination: {
        el: ".swiper-pagination-pos",
        type: "fraction",
    },
    breakpoints: {
        // when window width is >= 320px
        320: {
            slidesPerView: 3.1,
            spaceBetween: 10
        },
        // when window width is >= 480px
        480: {
            slidesPerView: 3.1,
            spaceBetween: 10
        },
        // when window width is >= 640px
        640: {
            slidesPerView: 3.1,
            spaceBetween: 10
        },
        1080: {
            slidesPerView: 4,
            spaceBetween: 40
        }
    }
})
const swiperPop = new Swiper('.swiper-container-pop', {
    // Default parameters
    slidesPerView: "auto",
    watchSlidesVisibility: true,
    spaceBetween: 10,
})